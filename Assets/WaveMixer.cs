﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class WaveMixer : MonoBehaviour
{
	public Toggle wave1Toggle;
	public Toggle wave2Toggle;
	public Toggle wave3Toggle;
	public Toggle wave4Toggle;
	public Toggle wave5Toggle;
	public Toggle wave6Toggle;
	public Toggle wave7Toggle;
	public Toggle wave8Toggle;

	public Slider groundSpeedSlider;
	public Slider tickIntervalSlider;
	public Slider tickTimeSlider;
	public Slider tickDistanceSlider;
	
	public Slider waveHeightSlider;
	public Slider waveTimeSlider;
	public Slider waveIntervalSlider;

	public GameObject waveButtonPanel;
	public GameObject waveSliderPanel;
	public GameObject groundSliderPanel;
	
	private bool isVisible = true;
	
	
	public delegate void OnWaveActiveChangedDelegate(int waveIndex, bool isActive);
	public OnWaveActiveChangedDelegate OnWaveChanged;
	
	public delegate void OnWaveHeightChangedDelegate(float height);
	public OnWaveHeightChangedDelegate OnWaveHeightChanged;
	
	public delegate void OnWaveTimeChangedDelegate(float time);
	public OnWaveTimeChangedDelegate OnWaveTimeChanged;
	
	public delegate void OnWaveIntervalChangedDelegate(float interval);
	public OnWaveIntervalChangedDelegate OnWaveIntervalChanged;
	
	
	public delegate void OnGroundSpeedChangedDelegate(float speed);
	public OnGroundSpeedChangedDelegate OnGroundSpeedChanged;
	
	public delegate void OnTickIntervalChangedDelegate(float interval);
	public OnTickIntervalChangedDelegate OnTickIntervalChanged;
	
	public delegate void OnTickTimeChangedDelegate(float time);
	public OnTickTimeChangedDelegate OnTickTimeChanged;
	
	public delegate void OnTickDistanceChangedDelegate(float distance);
	public OnTickDistanceChangedDelegate OnTickDistanceChanged;



	public void OnEnable()
	{
		wave1Toggle.onValueChanged.AddListener(delegate { WaveToggled(0, wave1Toggle.isOn); });
		wave2Toggle.onValueChanged.AddListener(delegate { WaveToggled(1, wave2Toggle.isOn); });
		wave3Toggle.onValueChanged.AddListener(delegate { WaveToggled(2, wave3Toggle.isOn); });
		wave4Toggle.onValueChanged.AddListener(delegate { WaveToggled(3, wave4Toggle.isOn); });
		wave5Toggle.onValueChanged.AddListener(delegate { WaveToggled(4, wave5Toggle.isOn); });
		wave6Toggle.onValueChanged.AddListener(delegate { WaveToggled(5, wave6Toggle.isOn); });
		wave7Toggle.onValueChanged.AddListener(delegate { WaveToggled(6, wave7Toggle.isOn); });
		wave8Toggle.onValueChanged.AddListener(delegate { WaveToggled(7, wave8Toggle.isOn); });
		
		groundSpeedSlider.onValueChanged.AddListener(delegate { GroundSpeedChanged(groundSpeedSlider.value); });
		tickIntervalSlider.onValueChanged.AddListener(delegate { TickIntervalChanged(tickIntervalSlider.value); });
		tickTimeSlider.onValueChanged.AddListener(delegate { TickTimeChanged(tickTimeSlider.value); });
		tickDistanceSlider.onValueChanged.AddListener(delegate { TickDistanceChanged(tickDistanceSlider.value); });
		
		waveHeightSlider.onValueChanged.AddListener(delegate { WaveHeightChanged(waveHeightSlider.value); });
		waveTimeSlider.onValueChanged.AddListener(delegate { WaveTimeChanged(waveTimeSlider.value); });
		waveIntervalSlider.onValueChanged.AddListener(delegate { WaveIntervalChanged(waveIntervalSlider.value); });
		
		CityEvents.OnUIToggled += CityEvents_OnUIToggled;
	}
	
	public void OnDisable()
	{
		wave1Toggle.onValueChanged.RemoveAllListeners();
		wave2Toggle.onValueChanged.RemoveAllListeners();
		wave3Toggle.onValueChanged.RemoveAllListeners();
		wave4Toggle.onValueChanged.RemoveAllListeners();
		wave5Toggle.onValueChanged.RemoveAllListeners();
		wave6Toggle.onValueChanged.RemoveAllListeners();
		wave7Toggle.onValueChanged.RemoveAllListeners();
		wave8Toggle.onValueChanged.RemoveAllListeners();
		
		groundSpeedSlider.onValueChanged.RemoveAllListeners();
		tickIntervalSlider.onValueChanged.RemoveAllListeners();
		tickTimeSlider.onValueChanged.RemoveAllListeners();
		tickDistanceSlider.onValueChanged.RemoveAllListeners();
		
		waveHeightSlider.onValueChanged.RemoveAllListeners();
		waveTimeSlider.onValueChanged.RemoveAllListeners();
		waveIntervalSlider.onValueChanged.RemoveAllListeners();
		
		CityEvents.OnUIToggled -= CityEvents_OnUIToggled;
	}

	private void CityEvents_OnUIToggled()
	{
		isVisible = !isVisible;

		Debug.Log("CityEvents_OnUIToggled: " + isVisible);
			
		waveButtonPanel.SetActive(isVisible);
		waveSliderPanel.SetActive(isVisible);
		groundSliderPanel.SetActive(isVisible);
	}


	public void Init()
	{
		// init as per inspector values
		OnWaveHeightChanged?.Invoke(waveHeightSlider.value);
		OnWaveTimeChanged?.Invoke(waveTimeSlider.value);
		OnWaveIntervalChanged?.Invoke(waveIntervalSlider.value);
		
		OnGroundSpeedChanged?.Invoke(groundSpeedSlider.value);
		OnTickIntervalChanged?.Invoke(tickIntervalSlider.value);
		OnTickTimeChanged?.Invoke(tickTimeSlider.value);
		OnTickDistanceChanged?.Invoke(tickDistanceSlider.value);

		OnWaveChanged?.Invoke(0, wave1Toggle.isOn);
		OnWaveChanged?.Invoke(1, wave2Toggle.isOn);
		OnWaveChanged?.Invoke(2, wave3Toggle.isOn);
		OnWaveChanged?.Invoke(3, wave4Toggle.isOn);
		OnWaveChanged?.Invoke(4, wave5Toggle.isOn);
		OnWaveChanged?.Invoke(5, wave6Toggle.isOn);
		OnWaveChanged?.Invoke(6, wave7Toggle.isOn);
		OnWaveChanged?.Invoke(7, wave8Toggle.isOn);
	}

	private void WaveHeightChanged(float value)
	{
		OnWaveHeightChanged?.Invoke(value);
	}
	
	private void WaveTimeChanged(float value)
	{
		OnWaveTimeChanged?.Invoke(value);
	}
	
	private void WaveIntervalChanged(float value)
	{
		OnWaveIntervalChanged?.Invoke(value);
	}


	private void GroundSpeedChanged(float value)
	{
		OnGroundSpeedChanged?.Invoke(value);
	}
	
	private void TickIntervalChanged(float value)
	{
		OnTickIntervalChanged?.Invoke(value);
	}
	
	private void TickTimeChanged(float value)
	{
		OnTickTimeChanged?.Invoke(value);
	}

	private void TickDistanceChanged(float value)
	{
		OnTickDistanceChanged?.Invoke(value);
	}


	private void WaveToggled(int waveIndex, bool isOn)
	{
		OnWaveChanged?.Invoke(waveIndex, isOn);
	}
}
