﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CityEvents
{
	public delegate void OnUIToggledDelegate();
	public static OnUIToggledDelegate OnUIToggled;
	
	//public delegate void OnBlockVisibleDelegate(CityBlock cityBlock);
	//public static OnBlockVisibleDelegate OnBlockVisible;
	
	//public delegate void OnBlockInvisibleDelegate(CityBlock cityBlock);
	//public static OnBlockInvisibleDelegate OnBlockInvisible;
	
	//public delegate void OnWaveActiveChangedDelegate(int waveIndex, bool isActive);
	//public static OnWaveActiveChangedDelegate OnWaveChanged;
	
	//public delegate void OnWaveTweenChangedDelegate(int waveIndex, LeanTweenType tween);
	//public static OnWaveTweenChangedDelegate OnWaveTweenChanged;
	
	//public delegate void OnWaveHeightChangedDelegate(float height);
	//public static OnWaveHeightChangedDelegate OnWaveHeightChanged;
	
	//public delegate void OnWaveTimeChangedDelegate(float time);
	//public static OnWaveTimeChangedDelegate OnWaveTimeChanged;
	
	//public delegate void OnWaveIntervalChangedDelegate(float interval);
	//public static OnWaveIntervalChangedDelegate OnWaveIntervalChanged;
	
	
	//public delegate void OnGroundSpeedChangedDelegate(float speed);
	//public static OnGroundSpeedChangedDelegate OnGroundSpeedChanged;
	
	//public delegate void OnTickIntervalChangedDelegate(float interval);
	//public static OnTickIntervalChangedDelegate OnTickIntervalChanged;
	
	//public delegate void OnTickTimeChangedDelegate(float time);
	//public static OnTickTimeChangedDelegate OnTickTimeChanged;
	
	//public delegate void OnTickDistanceChangedDelegate(float distance);
	//public static OnTickDistanceChangedDelegate OnTickDistanceChanged;
}
