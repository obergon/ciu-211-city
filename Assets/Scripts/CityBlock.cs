﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityBlock : MonoBehaviour
{
	//private float minRiseUpPause = 0.01f;
	//private float maxRiseUpPause = 0.2f;
	private float belowGround = 0.1f;

	public List<Tower> towerPrefabs = new List<Tower>();
	public List<Material> towerMaterials = new List<Material>();
	
	public float towerSpacingZ = 2f;
	public float towerSpacingX = 2f;
	public Transform towerBuildPosition;

	public GameObject personPrefab;

	public bool randomTowerMaterial;

	public int towersWide = 5;
	public int towersDeep = 4;

	private float towerInterval; 		// can be changed 'on the fly'
	
	public List<List<Tower>> towerRows = new List<List<Tower>>();
	private Vector3 buildStartPosition;
	//public Transform blockRotatePosition;		// move to end of line when past this point

	public bool isLeftSide = false;
	

	public void Start()
	{
 		buildStartPosition = towerBuildPosition.transform.localPosition;
 		
 		if (isLeftSide)
 			BuildTowersLeft(towersDeep, towersWide);
 		else
			BuildTowersRight(towersDeep, towersWide);
	}


	private Material RandomMaterial()
	{
		return towerMaterials [ Random.Range(0, towerMaterials.Count) ];
	}

	private void BuildTowersRight(int deep, int wide)
	{
		Tower newTower;
		Vector3 newTowerPosition = towerBuildPosition.transform.localPosition;
		
		for (int x = 0; x < deep; x++)
		{		
			var towerRow = new List<Tower>();
			for (int z = 0; z < wide; z++)
			{
				newTower = Instantiate(towerPrefabs[x], this.transform);

				if (randomTowerMaterial)
					newTower.GetComponent<Renderer>().material = RandomMaterial();
					
				//Debug.Log("newTower height: " + newTower.TowerHeight);
				newTower.transform.localPosition = new Vector3(newTowerPosition.x + (towerSpacingX * x),
															newTowerPosition.y - newTower.TowerHeight - belowGround,
															newTowerPosition.z + (towerSpacingZ * z));
	
				towerRow.Add(newTower);
			}
			towerRows.Add(towerRow);
		}
	}
	
	private void BuildTowersLeft(int deep, int wide)
	{
		Tower newTower;
		Vector3 newTowerPosition = towerBuildPosition.transform.localPosition;
		
		for (int x = deep-1; x >=0; x--)
		{		
			var towerRow = new List<Tower>();
			//for (int z = wide-1; z >= 0; z--)
			for (int z = 0; z < wide; z++)
			{
				newTower = Instantiate(towerPrefabs[x], this.transform);

				if (randomTowerMaterial)
					newTower.GetComponent<Renderer>().material = RandomMaterial();
					
				newTower.transform.localPosition = new Vector3(newTowerPosition.x + (towerSpacingX * x),
															newTowerPosition.y - newTower.TowerHeight - belowGround,
															newTowerPosition.z + (towerSpacingZ * z));
				towerRow.Add(newTower);
			}
			towerRows.Add(towerRow);
		}
	}

	public void SetTowerInterval(float interval)
	{
		towerInterval = interval;
	}
	
	
	public void SetTowerHeight(float height)
	{
		foreach (var towerRow in towerRows)
		{
			foreach (var tower in towerRow)
			{
				tower.SetTowerRiseHeight(height);
			}
		}
	}
	
	public void SetTowerTime(float time)
	{
		foreach (var towerRow in towerRows)
		{
			foreach (var tower in towerRow)
			{
				tower.SetTowerRiseTime(time);
			}
		}
	}
	
	public void SetTowerDelay(float delay)
	{
		foreach (var towerRow in towerRows)
		{
			foreach (var tower in towerRow)
			{
				tower.SetTowerRiseDelay(delay);
			}
		}
	}
	
	public IEnumerator AnimateTowerRow(int rowNum, float distance, float riseTime, LeanTweenType tween, float interval, int waveIndex)
	{
		SetTowerInterval(interval);
		
		var towerRow = towerRows[rowNum];
		{
			foreach (var tower in towerRow)
			{
				tower.AnimateWave(distance, riseTime, interval, tween, true, waveIndex);
				yield return new WaitForSeconds(towerInterval);
			}
		}

		yield return null;
	}
	
	public IEnumerator AnimateTowerColumn(int colNum, float distance, float riseTime, LeanTweenType tween, float interval, int waveIndex)
	{
		//Debug.Log("AnimateTowerColumn: colNum " + colNum + ", towerRows.Count = " + towerRows.Count);
	
		SetTowerInterval(interval);
	
		foreach (var towerRow in towerRows)
		{	
			var tower = towerRow[colNum];
			{
				tower.AnimateWave(distance, riseTime, interval, tween, true, waveIndex);
				yield return new WaitForSeconds(towerInterval);
			}
		}

		yield return null;
	}

	public void ResetTowers()
	{
		foreach (var towerRow in towerRows)
		{
			foreach (var tower in towerRow)
			{
				tower.ResetPosition(false);
			}
		}
	}
	
	public void StopTowerAnimations()		// tweens
	{
		foreach (var towerRow in towerRows)
		{
			foreach (var tower in towerRow)
			{
				tower.CancelAllTweens();
			}
		}
	}
	
	
	public void StopWaveAnimations(int waveIndex)		// tweens
	{
		foreach (var towerRow in towerRows)
		{
			foreach (var tower in towerRow)
			{
				tower.CancelTween(waveIndex);
			}
		}
	}

}
