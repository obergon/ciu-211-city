﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityLooper : MonoBehaviour
{
	public CityBlock blockPrefab;
	public float blockSpacingZ;
	public float blockSpacingX;
	public Transform blockBuildPosition;			// build start position
	public Transform blockRotatePosition;		// move to end of line when past this point
	
	private Vector3 cityStartPosition;
	private Vector3 buildStartPosition;
	private int blocksDeep = 1;
	private int blocksWide = 7;
	
	private bool scrollPaused = false;
	private bool tickerPaused = false;
	private bool tickInProgress = false;
	
	private int rowsToAnimate = 5;          // per block
	private int colsToAnimate = 5;          // per block

	private bool citySetup = false;
	private bool settingUp = false;

	public bool leftSide;						// of street .... brain?
	public List<List<CityBlock>> blockRows = new List<List<CityBlock>>();
	//public List<Queue<CityBlock>> blockRows = new List<Queue<CityBlock>>();
	
	private float groundSpeed;

	private float tickInterval;
	private float tickTime;
	private float tickDistance;
	private LeanTweenType tickTween = LeanTweenType.easeInSine;
	
	private float towerRiseDistance = 10f;
	private float towerRiseTime = 1f;
	private float towerRiseInterval = 0f;

	private float sliderFactor = 10f;		// to allow slider to operate with whole ints to avoid too many changed events
	
	public WaveMixer mixer;
	
	public LeanTweenType wave1Tween;
	public LeanTweenType wave2Tween;
	public LeanTweenType wave3Tween;
	public LeanTweenType wave4Tween;
	public LeanTweenType wave5Tween;
	public LeanTweenType wave6Tween;
	public LeanTweenType wave7Tween;
	public LeanTweenType wave8Tween;
	
	//	flags for active waves
	private bool wave1;
	private bool wave2;
	private bool wave3;
	private bool wave4;
	private bool wave5;
	private bool wave6;
	private bool wave7;
	private bool wave8;


	private void Start()
	{
		cityStartPosition = transform.position;
		buildStartPosition = blockBuildPosition.transform.localPosition;
		
		BuildBlocks(blocksDeep, blocksWide);
		
		scrollPaused = true;
		
		LeanTween.init(3200);
	}


	// Start is called before the first frame update
	private void OnEnable()
   	{
		if (mixer != null)
		{
			mixer.OnWaveChanged += Mixer_OnWaveChanged;
			//mixer.OnWaveTweenChanged += Mixer_OnWaveTweenChanged;
			mixer.OnWaveHeightChanged += Mixer_OnWaveHeightChanged;
			mixer.OnWaveTimeChanged += Mixer_OnWaveTimeChanged;
			mixer.OnWaveIntervalChanged += Mixer_OnWaveIntervalChanged;
			
			mixer.OnGroundSpeedChanged += Mixer_OnGroundSpeedChanged;
			mixer.OnTickIntervalChanged += Mixer_OnTickIntervalChanged;
			mixer.OnTickTimeChanged += Mixer_OnTickTimeChanged;
			mixer.OnTickDistanceChanged += Mixer_OnTickDistanceChanged;
		}
	}
	
	
	private void OnDisable()
   	{
       	if (mixer != null)
		{
			mixer.OnWaveChanged -= Mixer_OnWaveChanged;
			//mixer.OnWaveTweenChanged -= Mixer_OnWaveTweenChanged;
			mixer.OnWaveHeightChanged -= Mixer_OnWaveHeightChanged;
			mixer.OnWaveTimeChanged -= Mixer_OnWaveTimeChanged;
			mixer.OnWaveIntervalChanged -= Mixer_OnWaveIntervalChanged;
			
			mixer.OnGroundSpeedChanged -= Mixer_OnGroundSpeedChanged;
			mixer.OnTickIntervalChanged -= Mixer_OnTickIntervalChanged;
			mixer.OnTickTimeChanged -= Mixer_OnTickTimeChanged;
			mixer.OnTickDistanceChanged -= Mixer_OnTickDistanceChanged;
		}
	}
	

	private void Update()
	{
		if (!scrollPaused && groundSpeed > 0)
		{
			transform.Translate(-Vector3.forward * groundSpeed * Time.deltaTime);
		}

		if (Input.GetKeyDown(KeyCode.Space))
		{
			if (!citySetup & !settingUp)
				CitySetup();
			else
				CityEvents.OnUIToggled?.Invoke();
		}
		
		CheckBlockBehindCamera();
	}
	

	private void Mixer_OnWaveChanged(int waveIndex, bool isActive)
	{
		switch (waveIndex)
		{
			case 0:
				wave1 = isActive;
				break;
			
			case 1:
				wave2 = isActive;
				break;
				
			case 2:
				wave3 = isActive;
				break;
				
			case 3:
				wave4 = isActive;
				break;
			
			case 4:
				wave5 = isActive;
				break;
				
			case 5:
				wave6 = isActive;
				break;
								
			case 6:
				wave7 = isActive;
				break;
								
			case 7:
				wave8 = isActive;
				break;				
		}
		
		if (isActive)        // not already tweening - start
			StartCoroutine(StartWaveAnimation(waveIndex));
		else 					// currently tweening - stop
			StopWaveAnimation(waveIndex);
			
		if (NoWave)			// all waves turned off
			RestartAnimation();
	}


	//private void Mixer_OnWaveTweenChanged(int waveIndex, LeanTweenType tween)
	//{
	//}


	private void Mixer_OnWaveHeightChanged(float height)
	{	
		towerRiseDistance = height / sliderFactor;

		//Debug.Log("OnWaveHeightChanged: " + towerRiseDistance);

		foreach (var row in blockRows)
		{
			foreach (var block in row)
			{
				block.SetTowerHeight(towerRiseDistance);
			}
		}
	}


	void Mixer_OnWaveTimeChanged(float time)
	{
		towerRiseTime = time / sliderFactor;
	
		foreach (var row in blockRows)
		{
			foreach (var block in row)
			{
				block.SetTowerTime(towerRiseTime);
			}
		}
	}


	private void Mixer_OnWaveIntervalChanged(float interval)
	{
		towerRiseInterval = interval / sliderFactor;
	
		foreach (var row in blockRows)
		{
			foreach (var block in row)
			{
				block.SetTowerDelay(towerRiseInterval);
			}
		}
	}


	private void Mixer_OnGroundSpeedChanged(float speed)
	{
		groundSpeed = speed / sliderFactor;
	}


	private void Mixer_OnTickIntervalChanged(float interval)
	{
		tickInterval = interval / sliderFactor;
	}


	private void Mixer_OnTickTimeChanged(float time)
	{
		tickTime = time / sliderFactor;
	}


	private void Mixer_OnTickDistanceChanged(float distance)
	{
		tickDistance = distance / sliderFactor;
	}



	// quick & dirty check every frame as OnBecameInvisible not reliable in this instance
	private void CheckBlockBehindCamera()
	{
		foreach (var row in blockRows)
		{
			foreach (var block in row)
			{
				if (block.transform.position.z <= blockRotatePosition.position.z) // Camera.main.transform.position.z)
				{
					MoveBlockToBack(block, row);
					return;
				}
			}
		}
	}
	
	
	private void MoveBlockToBack(CityBlock cityBlock, List<CityBlock> row) // , Queue<CityBlock> row)
	{
		var blockPosition = cityBlock.transform.localPosition;	
		cityBlock.transform.localPosition = new Vector3(blockPosition.x, blockPosition.y,
															blockPosition.z + (blockSpacingZ * row.Count));

		// move block to end of queue
		//row.Dequeue();
		//row.Enqueue(cityBlock);
	}
    
 
 	private void BuildBlocks(int deep, int wide)
	{
		CityBlock newBlock;
		Vector3 newBlockPosition = blockBuildPosition.transform.localPosition;
		
		for (int x = 0; x < deep; x++)
		{
			var blockRow = new List<CityBlock>();
			
			for (int z = 0; z < wide; z++)
			{
				newBlock = Instantiate(blockPrefab, this.transform);
				newBlock.transform.localPosition = new Vector3(newBlockPosition.x + (blockSpacingX * x),
													newBlockPosition.y, newBlockPosition.z + (blockSpacingZ * z));

				newBlock.isLeftSide = leftSide;
	
				blockRow.Add(newBlock);
			}
			blockRows.Add(blockRow);
		}
	}
	
		
	private IEnumerator AnimateTowerRows(int rowNum, float distance, float riseTime, LeanTweenType tween, float delay, int waveIndex)
	{		
		foreach (var blockRow in blockRows)
		{
			foreach (var block in blockRow)
			{
				StartCoroutine(block.AnimateTowerRow(rowNum, distance, riseTime, tween, delay, waveIndex));
			}
		}
		yield return null;
	}
	
	
	private IEnumerator AnimateTowerRowInTurn(int rowNum, float distance, float riseTime, LeanTweenType tween, float delay, int waveIndex)
	{		
		foreach (var blockRow in blockRows)
		{
			foreach (var block in blockRow)
			{
				yield return StartCoroutine(block.AnimateTowerRow(rowNum, distance, riseTime, tween, delay, waveIndex));
			}
		}
		yield return null;
	}



	private IEnumerator AnimateTowerColumns(int colNum, float distance, float riseTime, LeanTweenType tween, float interval, int waveIndex)
	{		
		foreach (var blockRow in blockRows)
		{
			foreach (var block in blockRow)
			{
				StartCoroutine(block.AnimateTowerColumn(colNum, distance, riseTime, tween, interval, waveIndex));
			}
		}
		yield return null;
	}
	
	private IEnumerator AnimateTowerColumnInTurn(int colNum, float distance, float riseTime, LeanTweenType tween, float interval, int waveIndex)
	{		
		foreach (var blockRow in blockRows)
		{
			foreach (var block in blockRow)
			{
				yield return StartCoroutine(block.AnimateTowerColumn(colNum, distance, riseTime, tween, interval, waveIndex));
			}
		}
		yield return null;
	}
	

	private void CitySetup()
	{
		if (citySetup)
			return;
			
		if (settingUp)
			return;
		
		scrollPaused = true;
		settingUp = true;
		
		if (mixer != null)
			mixer.Init();

		CityAnimation();
		StartCoroutine(CityTicker());

		settingUp = false;
		citySetup = true;
		scrollPaused = false;
	}


	private void RestartAnimation()
	{
		StopAllCoroutines();
		
		foreach (var blockRow in blockRows)
		{
			foreach (var block in blockRow)
			{
				block.StopAllCoroutines();
				block.StopTowerAnimations();
				block.ResetTowers();
			}
		}
		
		CityAnimation();
		StartCoroutine(CityTicker());
	}


	private void CityAnimation()
	{
		if (wave1)
		{
			StartCoroutine(StartWaveAnimation(0));			
		}

		if (wave2)
		{
			StartCoroutine(StartWaveAnimation(1));			
		}


		if (wave3)
		{
			StartCoroutine(StartWaveAnimation(2));
		}

		if (wave4)
		{
			StartCoroutine(StartWaveAnimation(3));
		}

		if (wave5)
		{
			StartCoroutine(StartWaveAnimation(4));		
		}

		if (wave6)
		{
			StartCoroutine(StartWaveAnimation(5));			
		}

		if (wave7)
		{
			StartCoroutine(StartWaveAnimation(6));
		}

		if (wave8)
		{
			StartCoroutine(StartWaveAnimation(7));
		}
	}
	

	private IEnumerator StartWaveAnimation(int waveIndex)
	{
		//Debug.Log("StartWaveAnimation: " + waveIndex);
	
		switch (waveIndex)
		{
			case 0:
			{
				for (int i = 0; i < colsToAnimate; i++)
					yield return StartCoroutine(AnimateTowerColumnInTurn(i, towerRiseDistance, towerRiseTime, wave1Tween, towerRiseInterval, 0));	
				break;
			}

			case 1:
			{
				for (int i = 0; i < colsToAnimate; i++)
					StartCoroutine(AnimateTowerColumnInTurn(i, towerRiseDistance, towerRiseTime, wave2Tween, towerRiseInterval, 1));			
				break;
			}

			case 2:
			{
				for (int i = 0; i < colsToAnimate; i++)
					yield return StartCoroutine(AnimateTowerColumns(i, towerRiseDistance, towerRiseTime, wave3Tween, towerRiseInterval, 2));		
				break;
			}

			case 3:
			{
				for (int i = 0; i < colsToAnimate; i++)
					StartCoroutine(AnimateTowerColumns(i, towerRiseDistance, towerRiseTime, wave4Tween, towerRiseInterval, 3));				
				break;
			}

			case 4:
			{
				for (int i = 0; i < rowsToAnimate; i++)
					yield return StartCoroutine(AnimateTowerRowInTurn(i, towerRiseDistance, towerRiseTime, wave5Tween, towerRiseInterval, 4));
				break;
			}

			case 5:
			{
				for (int i = 0; i < rowsToAnimate; i++)
					StartCoroutine(AnimateTowerRowInTurn(i, towerRiseDistance, towerRiseTime, wave6Tween, towerRiseInterval, 5));	
				break;
			}

			case 6:
			{
				for (int i = 0; i < rowsToAnimate; i++)
					yield return StartCoroutine(AnimateTowerRows(i, towerRiseDistance, towerRiseTime, wave7Tween, towerRiseInterval, 6));
				break;
			}

			case 7:
			{
				for (int i = 0; i < rowsToAnimate; i++)
					StartCoroutine(AnimateTowerRows(i, towerRiseDistance, towerRiseTime, wave8Tween, towerRiseInterval, 7));
				break;
			}
		}
		
		yield return null;
	}

	
	private void StopWaveAnimation(int waveIndex)
	{
		//Debug.Log("StopWaveAnimation: " + waveIndex);
			
		foreach (var blockRow in blockRows)
		{
			foreach (var block in blockRow)
			{
				block.StopWaveAnimations(waveIndex);
			}
		}
	}
	
		
	private IEnumerator CityTicker()
	{
		while (! tickerPaused)
		{
			if (tickInProgress)
				yield return null;
		
			yield return new WaitForSeconds(tickInterval);
		
			if (tickTime > 0 && tickDistance > 0)
			{
				LeanTween.moveZ(gameObject, transform.position.z - tickDistance, tickTime).setEase(tickTween)
						.setOnStart(() => { tickInProgress = true; })
						.setOnComplete(() => { tickInProgress = false; });
			}
		}
	}

	
	private bool NoWave
	{
		get { return (!wave1 && !wave2 && !wave3 && !wave4 && !wave5 && !wave6 && !wave7 && !wave8); }
	}
}
