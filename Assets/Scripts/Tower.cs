﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
	public float minRiseUpTime = 0.1f;
	public float maxRiseUpTime = 0.5f;
	
	private Vector3 origPosition;
	private float resetTime = 0.2f;
	private LeanTweenType resetTween = LeanTweenType.easeOutExpo;
	//private List<int> tweenIds = new List<int>();

	private const int maxTweens = 8;
	private readonly int[] waveTweenIds = new int[ maxTweens ]; 		// one slot for each tweenId
	
	public float TowerHeight { get { return meshRenderer.bounds.extents.y * 2.0f; } }
	
	private Renderer meshRenderer;

	private LeanTweenType towerTween;
	private float tweenHeight;
	private float tweenTime;
	private float tweenDelay;


	void Awake()
	{
		meshRenderer = GetComponent<Renderer>();
	}

	private void Start()
	{
		origPosition = transform.localPosition;
		CancelAllTweens();
		//tweenIds.Clear();
    }

	public void ResetPosition(bool immediate)
	{
		if (immediate)
			transform.localPosition = origPosition;
		else
			LeanTween.moveLocalY(gameObject, origPosition.y, resetTime).setEase(resetTween);
	}
	
	//public void StopAnimation()
	//{
	//	foreach (int tweenId in tweenIds)
	//	{
	//		if (LeanTween.isTweening(tweenId))
	//			LeanTween.cancel(tweenId);
	//	}

	//	tweenIds.Clear();
	//}
	

	public void CancelAllTweens()
	{
		for (int i = 0; i < maxTweens; i++)
		{
			CancelTween(i);
		}
	}
	
	public void CancelTween(int waveIndex)
	{
		if (waveTweenIds[waveIndex] != 0)
		{
			if (LeanTween.isTweening(waveTweenIds[waveIndex]))
				LeanTween.cancel(waveTweenIds[waveIndex]);
			
			waveTweenIds[waveIndex] = 0;
		}
	}
	
	//public void PauseAllTweens()
	//{
	//	for (int i = 0; i < maxTweens; i++)
	//	{
	//		if (waveTweens[i] == 0)
	//			continue;

	//		LTDescr tween = LeanTween.descr(waveTweens[i]);
	
	//		if (tween != null)      // null if tween already finished
	//			tween.pause();
	//	}
	//}
	
	//public void ResumeAllTweens()
	//{
	//	for (int i = 0; i < maxTweens; i++)
	//	{
	//		if (waveTweens[i] == 0)
	//			continue;

	//		LTDescr tween = LeanTween.descr(waveTweens[i]);
	
	//		if (tween != null)      // null if tween already finished
	//			tween.resume();
	//	}
	//}


	public void SetupAnimation(LeanTweenType tween, float distance, float time, float delay)
	{
		towerTween = tween;
		tweenHeight = distance;
		tweenTime = time;
		tweenDelay = delay;
	}
	
	
	public void AnimateWave(float distance, float time, float delay, LeanTweenType tween, bool loop, int waveIndex)
	{
		SetupAnimation(tween, distance, time, delay);
			
		CancelTween(waveIndex);
		//PauseAllTweens();

		LoopTween(waveIndex);
	}


	private void LoopTween(int waveIndex)
	{
		waveTweenIds[waveIndex] = LeanTween.moveLocalY(gameObject, origPosition.y + tweenHeight, tweenTime)
								.setEase(towerTween).setDelay(tweenDelay)
								.setLoopPingPong(1).id;
								
		LTDescr tween = LeanTween.descr(waveTweenIds[waveIndex]);
		tween.setOnComplete(() =>					
		{
			LoopTween(waveIndex);
		});
	}
	
	//private void LoopTween(int waveIndex)
	//{
	//	waveTweenIds[waveIndex] = LeanTween.moveLocalY(gameObject, origPosition.y + tweenHeight, tweenTime)
	//							.setEase(towerTween).setDelay(tweenDelay)
	//							.setLoopPingPong().id;
	//}


	public void SetTowerRiseHeight(float newHeight)
	{
		tweenHeight = newHeight;
		
		//for (int i = 0; i < maxTweens; i++)
		//{
		//	SetTowerHeight(newHeight, i);
		//}
	}

	public void SetTowerRiseTime(float newTime)
	{
		tweenTime = newTime;
		
		//for (int i = 0; i < maxTweens; i++)
		//{
		//	SetTowerTime(newTime, i);
		//}
	}
	
	public void SetTowerRiseDelay(float delay)
	{
		tweenDelay = delay;
		
		//for (int i = 0; i < maxTweens; i++)
		//{
		//	SetTowerDelay(delay, i);
		//}
	}
	
	
	//private void SetTowerHeight(float newHeight, int waveIndex)
	//{
	//	if (waveTweenIds[waveIndex] == 0)
	//		return;

	//	Debug.Log("SetTowerHeight: newHeight = " + newHeight + " waveIndex = " + waveIndex);

	//	//ResetPosition();

	//	//LeanTween.moveLocalY(gameObject, origPosition.y, resetTime).setEase(resetTween).setOnComplete(() =>
	//	{
	//		LTDescr tween = LeanTween.descr(waveTweenIds[waveIndex]);

	//		//if (tween != null)      // null if tween already finished
	//		{
	//			//tween.setFrom(origPosition);
	//			tween.setTo(new Vector3(origPosition.x, origPosition.y + newHeight, origPosition.z));
	//		}
	//	} //);
	//}
	
	//private void SetTowerTime(float newTime, int waveIndex)
	//{
	//	if (waveTweenIds[waveIndex] == 0)
	//		return;

	//	LTDescr tween = LeanTween.descr(waveTweenIds[waveIndex]);

	//	if (tween != null) 		// null if tween already finished
	//		tween.setTime(newTime);
	//}
		
	//private void SetTowerDelay(float newDelay, int waveIndex)
	//{
	//	if (waveTweenIds[waveIndex] == 0)
	//		return;

	//	LTDescr tween = LeanTween.descr(waveTweenIds[waveIndex]);

	//	if (tween != null) 		// null if tween already finished
	//		tween.setDelay(newDelay);
	//}
}
